package Business;

import Business.DiseasesDrugs.DiseasesDirectory;
import Business.DiseasesDrugs.VaccinationDirectory;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Enterprise.OrganRepository;
import Business.NGO.VolunteerDirectory;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;
import Business.Organ.OrganCatalogue;

/**
 *
 * @author Administrator
 */
public class EcoSystem extends Organization {

    private static EcoSystem business;
    private ArrayList<Network> networkList;
    private DiseasesDirectory diseasesDirectory;
    private VaccinationDirectory vaccinationDirectory;
    private WorkQueue workQueue;
    private OrganCatalogue organCatalogue;
    private VolunteerDirectory VolunteerDirectory;

    public VolunteerDirectory getVolunteerDirectory() {
        return VolunteerDirectory;
    }

    public void setVolunteerDirectory(VolunteerDirectory VolunteerDirectory) {
        this.VolunteerDirectory = VolunteerDirectory;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<>();
        vaccinationDirectory = new VaccinationDirectory();
        diseasesDirectory = new DiseasesDirectory();
        workQueue = new WorkQueue();
        organCatalogue = new OrganCatalogue();
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

    public DiseasesDirectory getDiseasesDirectory() {
        return diseasesDirectory;
    }

    public void setDiseasesDirectory(DiseasesDirectory diseasesDirectory) {
        this.diseasesDirectory = diseasesDirectory;
    }

    public VaccinationDirectory getVaccinationDirectory() {
        return vaccinationDirectory;
    }

    public void setVaccinationDirectory(VaccinationDirectory vaccinationDirectory) {
        this.vaccinationDirectory = vaccinationDirectory;
    }

    public boolean checkIfUsernameIsUnique(String username) {

        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
            return false;
        }

        return true;
    }

    public Enterprise getEnterprise(UserAccount uc) {
        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                        if (ua == uc) {
                            return enterprise;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Network getNetwork(UserAccount uc) {
        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (UserAccount ua : organization.getUserAccountDirectory().getUserAccountList()) {
                        if (ua == uc) {
                            return network;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Organization getOrganization(Employee  e) {
        for (Network network : networkList) {
            for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    for (Employee emp : organization.getEmployeeDirectory().getEmployeeList()) {
                        if (emp == e) {
                            return organization;
                        }
                    }
                }
            }
        }
        return null;
    }

    public Network getNetworkByName(String networkName) {

        for (Network network : networkList) {
            if (network.getName().equals(networkName)) {
                return network;
            }
        }
        return null;
    }

    public OrganCatalogue getOrganCatalogue() {
        return organCatalogue;
    }

    public void setOrganCatalogue(OrganCatalogue organCatalogue) {
        this.organCatalogue = organCatalogue;
    }

}
