/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.NGO;

import static Business.Role.Role.RoleType.Donor;
import java.util.ArrayList;

/**
 *
 * @author amandeep
 */
public class DonorDirectory {
    private ArrayList<Donor> donorDirectory;
    
    public DonorDirectory(){
        donorDirectory = new ArrayList();
        
    }

    public ArrayList<Donor> getDonorDirectory() {
        return donorDirectory;
    }
    public Donor addDonor() {
        Donor donor = new Donor();
        donorDirectory.add(donor);
        return donor;
        
        
    }
  
    }
    

