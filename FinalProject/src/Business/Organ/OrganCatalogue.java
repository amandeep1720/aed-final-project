/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organ;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author shreya
 */
public class OrganCatalogue {
    
    private ArrayList<String> organNames;
    private ArrayList<String> genderList;
    private ArrayList<String> bloodTypeList;
    private ArrayList<Organ> OrganRequestList;
    
    public OrganCatalogue(){
    
        OrganRequestList=new ArrayList();
      organNames=new ArrayList(Arrays.asList("Heart", "Kidney","Intestine","Eye", "Liver", "Tissue"));
        genderList=new ArrayList(Arrays.asList("Male","Female"));
        bloodTypeList=new ArrayList(Arrays.asList("A+","A-","B+","O+","O-","AB+","AB-","Bombay"));
    
    } 
    
    public Organ addOrgan()
    {
        Organ organRequest=new Organ();
        OrganRequestList.add(organRequest);
        return organRequest;
    }
    
    public void removeOrgan(Organ organRequest)
    {
        OrganRequestList.remove(organRequest);
    }

    public ArrayList<Organ> getOrganRequestList() {
        return OrganRequestList;   
    }

    public ArrayList<String> getOrganNames() {
        return organNames;
    }

    public ArrayList<String> getGenderList() {
        return genderList;
    }

    public ArrayList<String> getBloodTypeList() {
        return bloodTypeList;
    }
    
    
    
}
